# Дана довільна строка. Напишіть код, який знайде в ній і віведе на екран кількість слів, які містять дві голосні літери підряд.
# Є два довільних числа які відповідають за мінімальну і максимальну ціну. Є Dict з назвами магазинів і цінами: { "cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324, "x-store": 37.166, "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}. Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких попадають в діапазон між мінімальною і максимальною ціною. Наприклад:

lower_limit = 35.9
upper_limit = 37.339

my_dict = {"cito": 47.999,
          "BB_studio" : 42.999,
          "momo": 49.999,
          "main-service": 37.245,
          "buy.now": 38.324,
          "x-store": 37.166,
          "the_partner": 38.988,
          "store": 37.720,
          "rozetka": 38.003}

for dict_key, dict_value in my_dict.items():
    if 35.9 <= dict_value <= 37.339:
        print(dict_key)

word = input('Напишить своє слово: ')
glas_1 = 'aeiouAEIOU'
count = 0
jim = 0

for wo in word.split():
    wo.isalpha()
    for char in wo:
        if char in glas_1:
            jim += 1
            if jim >= 2:
                count += 1
                break
        else:
            jim = 0

print(count)